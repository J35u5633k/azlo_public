# python3
'''
Main purpose, to make up for the lack of transaction alerts offered by Azlo.
NOTE: There was no effort for making this universal...it was built for my specific use case. Feel free to use/adapt though.
Why send alerts for a balance diff? Azlo shows temporary holds immediately after a transaction in the web interface, but not the API. Customer support said, "We do not have 'temporary holds' as one of our end-points. I think the idea was that the posted and current balance would reflect those holds, but if you have several holds, that could be limited information and I can understand how that having 'temp-holds' would make for ease of use. I will forward this information along to my product team."

0,15,30,45 * * * * /usr/bin/python3 /path/monitorAzloAcct.py >> /path/monitorAzloAcct.log 2>&1

TODO
Sort returend transactions (postedDate > TransDate > Description). The transactions may not always be returned in the same (chronological) order...even though they were in testing.
More detailed status_code checks
Send email (and maybe txt) on error. Maybe create an error function to catch all errors? sample code (commented out) can be used to exclude/include SMS email addresses


'''

import requests
import json
import sys
import smtplib
import re
from datetime import datetime, timedelta
from email.message import EmailMessage

from config import API_KEY
from config import fromEmail
from config import fromEmailPasswd
from config import toEmailAddresses
from config import smtpHost
from config import smtpPort

# global constants
BASE_URL = "https://developer-api.azlo.com"
REQUESTS_HEADERS = {"Authorization" : API_KEY,
          "Accept" : "application/json"}
NOW = datetime.now()
YESTERDAY = NOW - timedelta(days=1)

# global variables
AcctID = ""
postedBalance = 0
currentBalance = 0
totalAlertValue = 0
sentATransactionAlert = False


try:
    with open("lastSentTransaction", 'r') as f:
        lastSentTransaction = f.read()
except Exception as e:
        #print("ERROR: lastSentTransaction file: %s" % e)
        lastSentTransaction = ""
try:
    with open("lastSeenPostedBalance", 'r') as f:
        lastSeenPostedBalance = round(float(f.read()),2)
except:
        #print("ERROR: lastSeenPostedBalance file: %s" % e)
        lastSeenPostedBalance = 0.0
try:
    with open("lastSeenCurrentBalance", 'r') as f:
        lastSeenCurrentBalance = round(float(f.read()),2)
except:
        #print("ERROR: lastSeenCurrentBalance file: %s" % e)
        lastSeenCurrentBalance = 0.0



def getRequests(urlPath):
    try:
        url = "%s%s" % (BASE_URL, urlPath)
        r = requests.get(url, headers=REQUESTS_HEADERS)
        # add check here to check for things like "unauthorized"
        if r.status_code != 200:
            return json.loads('{"ERROR":"Did not receive a 200-ok"}')
        if not "data" in r.json():
            return json.loads('{"ERROR":"Did not receive keyword data"}')
        convertedJson = json.loads(r.text)
        return convertedJson
    except Exception as e:
        #raise
        return "ERROR, GET request: %s" % str(e)


def evalBalances():
    try:
        #if values are diff AND they are diff from last time, send alert and update file
        # current is lower than posted if there is a temporary hold or otherwise not-yet-posted transaction
        if postedBalance == lastSeenPostedBalance and currentBalance == lastSeenCurrentBalance:
            return "No diff in balances"

        diffInBalanceDifferenceFromLastTime = abs(round(abs(round(lastSeenPostedBalance - lastSeenCurrentBalance,2)) - abs(round(postedBalance - currentBalance,2)),2)) # am i overthinking this?
        print("diffInBalanceDifferenceFromLastTime: %s, totalAlertValue: %s" % (diffInBalanceDifferenceFromLastTime, totalAlertValue))

        if sentATransactionAlert: # if we've already sent an alert for a new transaction, then of course the balances will be different
            if (not diffInBalanceDifferenceFromLastTime == totalAlertValue) and (diffInBalanceDifferenceFromLastTime != 0):  # if the diff does NOT match the alert, then we still need to send an alert
                sendEmail("additionalBalanceDifference", str(round(abs(diffInBalanceDifferenceFromLastTime - totalAlertValue),2)), "unkDesc", "unkPurchaseDate")
        elif currentBalance < postedBalance:
            # then there is a temporary hold/charge
            #send alert about the pending charge
            #sendEmail("pendingChargeOrHold", str(round(postedBalance-currentBalance,2)), "unkDesc", "unkPurchaseDate")
            #try this, as we want to send the proper amount when there are 2+ pending charges
            sendEmail("pendingChargeOrHold", str(round(diffInBalanceDifferenceFromLastTime,2)), "unkDesc", "unkPurchaseDate")
        elif currentBalance > postedBalance: # will this ever happen? not sure
            # then there is (might be) a pending credit/deposit that is not yet available
            #send alert about the pending credit/deposit
            #sendEmail("pendingCredit", str(round(currentBalance-postedBalance,2)), "unkDesc", "unkPurchaseDate")
            #try this, as we want to send the proper amount when there are 2+ pending charges
            sendEmail("pendingCredit", str(round(diffInBalanceDifferenceFromLastTime,2)), "unkDesc", "unkPurchaseDate")
        elif currentBalance == postedBalance:
            #then they were different before, but now are caught up with each other
            sendEmail("Balances match now", currentBalance, "...current balance, but had been %s difference" % str(round(diffInBalanceDifferenceFromLastTime)), "unkPurchaseDate")
        if postedBalance != lastSeenPostedBalance:
            with open("lastSeenPostedBalance", 'w') as f:
                f.write(str(postedBalance))
        if currentBalance != lastSeenCurrentBalance:
            with open("lastSeenCurrentBalance", 'w') as f:
                f.write(str(currentBalance))
        return "complete"
    except Exception as e:
        #raise
        return str(e)


def getAcctBalanceAndSetAcctID():
    try:
        global AcctID, postedBalance, currentBalance
        result = getRequests("/v1/accounts")
        if "ERROR" in result:
            return result
        #print(result)
        if not "data" in result:
            return "ERROR: data not in returned in data"
        acctDetails = result["data"][0]
        #print(acctDetails)
        if not "account_id" in acctDetails:
            return "ERROR: account_id not in returned in acctDetails"
        AcctID = acctDetails["account_id"]
        #print(AcctID)
        if not "balance" in acctDetails:
            return "ERROR: balance not in returned in acctDetails"
        acctBalance = acctDetails["balance"]
        #print(acctBalance)
        if not "posted_balance" in acctBalance:
            return "ERROR: posted_balance not in returned in acctBalance"
        postedBalance = round(float(acctBalance["posted_balance"]),2)
        currentBalance = round(float(acctBalance["current_balance"]),2) # lower than posted if there is a temporary hold/charge
        return "postedBalance: %s, currentBalance: %s" % (postedBalance, currentBalance)
    except Exception as e:
        #raise
        return "ERROR: %s" % e

def processLatestTransactions():
    try:
        global lastSentTransaction
        result = getRequests("/v1/accounts/%s/transactions?posted_date_from=%s" % (AcctID, YESTERDAY.strftime('%Y-%m-%d')))
        #result = getRequests("/v1/accounts/%s/" % (AcctID))

        if "ERROR" in result:
            return result
        #print(result)
        if not "data" in result:
            return "ERROR: data not in returned in data"
        transactions = result["data"]
        
        #transactionsList = []
        #for transaction in transactions:
        #    transactionsList.append(transaction)
        #    print(transaction)
        #transactionsList.reverse()
        #for transaction in transactionsList:
        needToSetLastTransaction = True
        for transaction in transactions:
            #print(transaction)
            if "id" in transaction:
                #print(transaction["id"])
                if transaction["id"] == lastSentTransaction:
                    return "SUCCESS: updates are caught up"
            else:
                return "ERROR: 'id' not present"
            if "transaction_type" in transaction:
                transactionType = "Azlo: " + str(transaction["transaction_type"])
            else:
                transactionType = "Azlo: unkType"
            if "description" in transaction:
                description = transaction["description"]
            else:
                description = "unkDesc"
            if "amount" in transaction:
                amount = transaction["amount"]
            else:
                amount = "unkAmount"
            if "purchase_date" in transaction:
                purchaseDate = transaction["purchase_date"]
            else:
                purchaseDate = "unkPurchaseDate"

            
            #print("%s,%s,%s" % (transactionType, amount, description))
            sendEmail(transactionType, amount, description, purchaseDate)
            
            if needToSetLastTransaction:
                with open("lastSentTransaction", 'w') as f:
                    f.write(str(transaction["id"]))
                needToSetLastTransaction = False
            
        return "No new transactions"
    except:
        #raise
        return "ERROR: unknown"

def sendEmail(transactionType, amount, description, purchaseDate):
    global sentATransactionAlert
    global totalAlertValue
    sentATransactionAlert = True
    totalAlertValue += abs(round(float(amount),2))
    print("sending alert: %s:$%s:%s:%s" % (transactionType, amount, description, purchaseDate))
    #return
    
    for emailAddress in toEmailAddresses:
        #print(emailAddress)
        #isSMS = False
        #if re.search("^\d{10}", emailAddress):
        #    isSMS = True

        msg = EmailMessage()
        msg['Subject'] = transactionType
        msg['From'] = fromEmail
        msg['To'] = emailAddress
        msg.set_content("$%s\n%s\n%s" % (amount, description, purchaseDate))
        s = smtplib.SMTP(host=smtpHost, port=smtpPort)
        s.login(fromEmail, fromEmailPasswd)
        s.send_message(msg)
        s.quit()


print("%s: starting" % NOW.strftime('%Y-%m-%d@%T'))
result = getAcctBalanceAndSetAcctID()
print("getAcctBalanceAndSetAcctID: %s" % result)
if "ERROR" in result:
    sys.exit()
print("processLatestTransactions %s" % processLatestTransactions())
print("evalBalances: %s" % evalBalances())
print("%s: finished" % NOW.strftime('%Y-%m-%d@%T'))


