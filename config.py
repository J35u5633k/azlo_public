# Account Profile > Developer API > View Key
# If key is not present, chat with customer support to request it
API_KEY = "[KEY HERE]"

# SMTP settings
smtpHost ='smtp.whatever.com'
smtpPort=587
fromEmail = "what@ever.com"
fromEmailPasswd = "[LAME PASSWORD HERE]"

# can be a single email address, or multiple addresses.
# for SMS, use the 10-digit phone number @ appropriate SMS gateway address
toEmailAddresses = {
        "whatever@gmail.com",
        "8885551234@txt.att.net"
}
